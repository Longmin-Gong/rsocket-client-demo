package com.rsocket.client.rsockeclient.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Person {
    String id;
    String name;
    Integer age;
}
