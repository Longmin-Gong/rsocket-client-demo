package com.rsocket.client.rsockeclient.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.rsocket.RSocketRequester;

/**
 * RSocket client configuration bean.
 *
 * @author longmin
 */
@Configuration
public class RSocketClientConfiguration {
    /**
     * Create a {@link RSocketRequester} based on {@link RSocketRequester.Builder}
     * dataMimeType and metadataMimeType are all application/json
     *
     * @param builder builder
     * @return {@link RSocketRequester}
     */
    @Bean
    public RSocketRequester rSocketRequester(RSocketRequester.Builder builder) {
        return builder.connectTcp("localhost", 7000).block();
    }
}
