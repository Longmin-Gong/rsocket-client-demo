package com.rsocket.client.rsockeclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(
        scanBasePackages = {"com.rsocket"}
)
public class RsockeClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(RsockeClientApplication.class, args);
    }
}
