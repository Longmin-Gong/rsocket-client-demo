package com.rsocket.client.rsockeclient.scheduletasks;

import com.rsocket.client.rsockeclient.entity.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

/**
 * @author longmin
 */
@Component
@EnableScheduling
@Slf4j
public class DelaySendTask {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private RSocketRequester rSocketRequester;

    @Scheduled(fixedDelay = 20000)
    public void tryChannelMode() {
        String now = dateFormat.format(new Date());
        log.info("{} send channel mode", now);
        Person p1 = new Person("first", "longmin", 18);
        Person p2 = new Person("second", "longmin", 18);
        Person p3 = new Person("third", "longmin", 18);
        Flux<Person> persons = Flux.concat(Mono.just(p1),
                Mono.just(p2).delayElement(Duration.ofSeconds(3)),
                Mono.just(p3).delayElement(Duration.ofSeconds(8)))
                .doOnNext(p -> log.info("send {}", p));
        rSocketRequester.
                route("channel")
                .data(persons)
                .retrieveFlux(Person.class)
                .subscribe(res -> log.info("Response received: {}", res));
    }

    @Scheduled(fixedRate = 5000)
    public void tryFireAndForget() {
        String now = dateFormat.format(new Date());
        log.info("{} send request in fireAndForget mode", now);
        rSocketRequester
                .route("createPerson")
                .data(new Person("personId", "longmin", 18))
                .send();
    }

    @Scheduled(fixedRate = 5000)
    public void tryRequestResponseMode() {
        String now = dateFormat.format(new Date());
        log.info("{} send request in requestResponse mode", now);
        Mono<Person> personMono = rSocketRequester.route("person")
                .data(now)
                .retrieveMono(Person.class);
        log.info("{} get response {} in requestResponse mode", dateFormat.format(new Date()), personMono.block().toString());
    }


    @Scheduled(fixedRate = 5000)
    public void tryRequestAndStreamMode() {
        String now = dateFormat.format(new Date());
        log.info("{} send requestStream mode", now);
        rSocketRequester
                .route("createBatchPersons")
                .data(10)
                .retrieveFlux(Person.class)
                .subscribe(res -> log.info("Response received: {}", res));
    }
}
